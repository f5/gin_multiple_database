package router

import (
	"esc.show/multiple_database/internal/core"
	tenant_router "esc.show/multiple_database/tenants/router"
)

func Router(c *core.Core) {

	tenant_router.Router(c)

}
