package model

type Domain struct {
	ID       int64  `json:"id,omitempty"`
	Domain   string `json:"domain,omitempty"`
	TenantId string `json:"tenant_id,omitempty"`
}
