package model

import (
	"gorm.io/datatypes"
	"time"
)

type Tenant struct {
	ID        string            `json:"id,omitempty" gorm:"primaryKey;size:192"`
	Name      string            `json:"name,omitempty" gorm:"size:192"`
	StartAt   time.Time         `json:"start_at"`
	ExpiredAt time.Time         `json:"expired_at"`
	Status    int               `json:"status,omitempty" gorm:"size:1"`
	Data      datatypes.JSONMap `json:"data,omitempty"`
}
