package model

import "gorm.io/datatypes"

type OrderExcelDetail struct {
	ID        int64          `json:"id,omitempty"`
	RequestId string         `json:"request_id,omitempty"`
	No        string         `json:"no,omitempty"`
	Status    int            `json:"status,omitempty"`
	Errors    datatypes.JSON `json:"errors,omitempty"`
}
