package tenant_router

import (
	api_router "esc.show/multiple_database/api"
	"esc.show/multiple_database/internal/core"
	middlware_tenancy "esc.show/multiple_database/tenants/middlware"
)

func Router(c *core.Core) {
	c.Engine.Use(middlware_tenancy.Middleware(c))
	api_router.Router(c)
}
