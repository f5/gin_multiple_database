package middlware_tenancy

import (
	"esc.show/multiple_database/internal/core"
	services_tenant "esc.show/multiple_database/tenants/services"
	"github.com/gin-gonic/gin"
)

func Middleware(c *core.Core) gin.HandlerFunc {

	return func(ctx *gin.Context) {
		c.GinContext = ctx
		tenant := services_tenant.Tenant(c)
		services_tenant.Init(c, tenant.ID)
		c.TenantID = tenant.ID
	}

}
