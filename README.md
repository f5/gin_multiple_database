
## 多租户数据库自动切换Golang测试

安装方式

1. 安装Go环境,并确认开启GO111MODULE
2. 在项目目录执行 `go mod tidy` 安装相关依赖
3. 编辑`config.json`配置自己的数据库信息 
4. 执行 `go run .` 启动测试
5. 执行 `go build` 项目打包


> 开发环境采用Go 1.18.10