package config

import (
	"github.com/spf13/viper"
	"log"
)

func Load() {
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalln("配置文件读取失败:", err)
	}
}
