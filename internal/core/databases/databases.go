package databases

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
)

func RegisterDb(d *gorm.DB, conn string) *gorm.DB {
	db, err := gorm.Open(mysql.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
		return nil
	}
	db.Logger.LogMode(logger.Silent)
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatal(err)
		return nil
	}
	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(100)
	d = db.Debug()
	return d
}
