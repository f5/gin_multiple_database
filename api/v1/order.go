package api_v1

import (
	"esc.show/multiple_database/internal/core"
	"esc.show/multiple_database/tenants/model"
	"github.com/gin-gonic/gin"
)

type Order struct {
}

func NewOrder() *Order {
	return &Order{}
}

func (o *Order) Check(c *core.Core) gin.HandlerFunc {

	return func(ctx *gin.Context) {

		requestId := c.GinContext.Query("request_id")

		var orderExcelDetail model.OrderExcelDetail

		err := c.TenantDb.Where("request_id=?", requestId).First(&orderExcelDetail).Error
		if err != nil {
			ctx.JSON(200, gin.H{
				"code":    400,
				"message": "订单不存在",
			})
			return
		}

		ctx.JSON(200, gin.H{
			"code":    200,
			"message": "ok",
			"data":    orderExcelDetail,
		})
		return
	}
}
