package api_router

import (
	api_v1 "esc.show/multiple_database/api/v1"
	"esc.show/multiple_database/internal/core"
)

func Router(c *core.Core) {
	api := c.Engine.Group("/api/v1/")
	{
		api.Any("/order/check", api_v1.NewOrder().Check(c))

	}

}
