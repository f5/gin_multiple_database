package main

import (
	"esc.show/multiple_database/internal/config"
	"esc.show/multiple_database/internal/core"
	"esc.show/multiple_database/router"
)

func main() {

	config.Load()

	c := core.New()

	Hooks := make([]core.Hooks, 0)

	Hooks = append(Hooks, router.Router)

	c.AddHook(Hooks...)

	c.Run()

}
